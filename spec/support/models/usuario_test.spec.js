var mongoose = require('mongoose');
var Bicicleta = require('../../../models/bicicleta');
var Usuario = require('../../../models/usuario');
var Reserva = require('../../../models/reserva');

describe('Testing usuarios', function () {  
    describe("long asynchronous specs", function() {

        var originalTimeout;
    
        beforeEach(function() {
            originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
            jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
        });
    
        it("takes a long time", function(done) {
            setTimeout(function() {
                done();
            }, 9000);
        });
    
        afterEach(function() {
            jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        });
    });
     
    beforeEach(function(done) {
        mongoose.disconnect();

        var mongoDb = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDb, {useUnifiedTopology: true,useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console,'connection error'));
        db.once('open', function() {
            console.log('we are connected to test database');
           
            done();
        });
        
    });
    
    afterEach(function(done){
            Reserva.deleteMany({}, function(err,success){
                if(err) console.log(err);
                Usuario.deleteMany({}, function(err, succes){
                    if (err) console.log(err);
                    Bicicleta.deleteMany({},function(err, succes){
                        if (err) console.log(err);
                        done();
                });
            });
        });
    });

    describe('Cuando un usuario reserva una bici', () => {

        it ('debe existir la reserva', (done) => {
            const usuario = new Usuario ({nombre: "Miguel"});
            usuario.save();
            const bicicleta = new Bicicleta({code: 1, color: "verde", modelo: "urbano"});
            bicicleta.save();

            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta.id, hoy, mañana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                console.log(reservas[0]);
                expect(reservas.length).toBe(1);
                expect(reservas[0].diasDeReserva()).toBe(2);
                expect(reservas[0].bicicleta.code).toBe(1);
                expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                done();
            });
        });

        });
    });
});

var mongoose = require('mongoose');
var Bicicleta = require('../../../models/bicicleta');


describe('Testing Bicicletas', function () {  
    describe("long asynchronous specs", function() {

        var originalTimeout;
    
        beforeEach(function() {
            originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
            jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
        });
    
        it("takes a long time", function(done) {
            setTimeout(function() {
                done();
            }, 9000);
        });
    
        afterEach(function() {
            jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        });
    });
     
    beforeEach(function(done) {
        mongoose.disconnect();

        var mongoDb = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDb, {useUnifiedTopology: true,useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console,'connection error'));
        db.once('open', function() {
            console.log('we are connected to test database');
           
            done();
        });
        
    });
    
    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err,sucess){
            if (err) console.log(err);
          
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance( 1,"verde", "urbana", [-34.5,-54.1]);
    
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('Comienza vacia',(done) =>{
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                done();
            });            
        });
    });


    describe('Bicicleta.add',() => {
        it('agrega solo una bici', (done) => {
            var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbano"});
            Bicicleta.add(aBici, function(err,newBici){
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                   
                    done();
                    
                });
            });            
        });        
    });

       describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 1', (done)=> {
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
                Bicicleta.add(aBici, function(err,newBici){
                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({code: 2, color:"rojo", modelo: "urbana"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function (error, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });
    
});



/*var Bicicleta = require('../../../models/bicicleta');

beforeEach(()=> { Bicicleta.allBicis=[];});

describe('Bicicleta.allBicis', () => {
it('Comienza vacia', () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add',()=>{
it('Agregamos una',()=>{
    expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta('Rojo','BMX',[-34.609174, -58.642770]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.FindbyID',()=>{
    it('debe devolver la bici con id 1',() =>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var abici1 = new Bicicleta(1,'Rojo','BMX',[-34.609174, -58.642770]);
        var abici2 = new Bicicleta(2,'Verde','Urbana',[-34.609174, -58.642770]);

        Bicicleta.add(abici1);
        Bicicleta.add(abici2);

        var targetBici= Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(abici1.color);
        expect(targetBici.modelo).toBe(abici1.modelo);
    });
});
*/
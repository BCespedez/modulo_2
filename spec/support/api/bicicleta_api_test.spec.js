var mongoose    = require ('mongoose')
var Bicicleta = require('../../../models/bicicleta');
var request= require('request');
var server = require('../../../bin/www');

var base_url= "http://localhost:3000/api/bicicletas"

describe("Bicileta API", () => {
    describe("long asynchronous specs", function() {

        var originalTimeout;
    
        beforeEach(function() {
            originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
            jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
        });
    
        it("takes a long time", function(done) {
            setTimeout(function() {
                done();
            }, 9000);
        });
    
        afterEach(function() {
            jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
        });
    });
     
    beforeEach(function(done) {
        mongoose.disconnect();

        var mongoDb = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDb, {useUnifiedTopology: true,useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console,'connection error'));
        db.once('open', function() {
            console.log('we are connected to test database');
           
            done();
        });
        
    });
    
    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err,sucess){
            if (err) console.log(err);
          
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it("Status 200", (done)=> {
            request.get(base_url, function(error,response,body){
                var result =JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            })
        });
    });
    describe("POST BICICLETAS /create", ()=> {
        it("status 200", (done)=> {
            var headers = {'content-type': 'application/json'};
            var aBici = '{"Code": 30", "color": "rojo", "modelo": "urbana", "last": -34}'
            request.post({
                headers: headers,
                url:    base_url + '/create',
                body: aBici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("rojo");
                expect(bici.ubicacion[0]).toBe(-34);
                expect(bici.ubicacion[1]).toBe(-54);
                done();
           

            });
        });
    });

    describe('DELETE BICICLETAS /delete', () => {

    });
});
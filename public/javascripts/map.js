var mymap = L.map('mapid').setView([-34.5907,-58.6383], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiYmNlc3BlZGV6IiwiYSI6ImNrZWc3dXA2eDBjZTMycG1udWxjaXhsaHMifQ.4MR5ZNpjoiKChjIwhKhSGA'
}).addTo(mymap);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici) {
          L.marker(bici.ubicacion,{title: bici.id}).addTo(mymap);
        });
    }
})